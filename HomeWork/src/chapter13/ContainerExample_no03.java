package chapter13;


/*
 * 문제 2번
 * ContainerExample 클래스의 main() 메소드는 Container 제네릭 타입을 사용하고 있습니다. 
 * main() 메소드에서 사용하는 방법을 참고해서 Container 제네릭 타입을 선언해보세요.
 * */
public class ContainerExample_no03 {
    public static void main(String[] args) {
        Container_no03<String, String> container1 = new Container_no03<> ();
        container1.set("홍길동", "도적");
        String name = container1.getKey();
        String job = container1.getValue();
        
        Container_no03<String , Integer> container2 = new Container_no03<>();
        container2.set("홍길동", 35);
        String name2 = container2.getKey();
        int age = container2.getValue();
    }
}
