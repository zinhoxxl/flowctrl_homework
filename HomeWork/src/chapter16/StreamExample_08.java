package chapter16;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * 문제 8번
 * List 에 저장되어 있는 Member 요소를 직업별로 그룹핑해서 
 * Map<String,list<String>> 객체로 생성하려고 합니다.
 * 키는 Member 의 직업이고 값은  Member 의 이름으로 구성된 
 * List<String>입니다. 빈칸에 알맞은 코드를 작성해 보세요.
 * */
public class StreamExample_08 {
	public static void main(String[] args) {
		List<Member> list = Arrays.asList(
				new Member("홍길동", "개발자"), 
				new Member("김나리", "디자이너"), 
				new Member("신용권", "개발자")
		);
		
		Map<String, List<String>> groupingMap = list.stream()
				.collect(
						Collectors.groupingBy(
								Member::getJob,
								Collectors.mapping(Member::getName, Collectors.toList())
						)
				);
		
		System.out.print("[개발자] ");
		groupingMap.get("개발자").stream().forEach(s -> System.out.print(s + " ")); // [개발자] 홍길동 신용권 
		System.out.print("\n[디자이너] ");
		groupingMap.get("디자이너").stream().forEach(s -> System.out.print(s + " ")); // [디자이너] 김나리
		
	}
	
	static class Member {
		private String name;
		private String job;
		
		public Member(String name, String job) {
			this.name = name;
			this.job = job;
		}
		
		public String getName() {
			return name;
		}
		public String getJob() {
			return job;
		}
	}
}
