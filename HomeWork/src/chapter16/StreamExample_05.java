package chapter16;

import java.util.Arrays;
import java.util.List;

/*
 * 문제 5번
 * List 에 저장되어 있는 String 요소에서 대소문자와 상관없이 
 * "java" 라는 단어가 포함된 문자열만 필터링해서 출력하려고 합니다. 
 * 빈칸에 알맞은 코드를 작성해 보세요.
 * */
public class StreamExample_05 {
	public static void main(String[] args) {
		List<String> list = Arrays.asList(
					"This is a java book",
					"Lambda Expressions",
					"Java8 supports lambda expressions"
					);
		list.stream()
			.filter(a -> a.toLowerCase().contains("java")).
			forEach(a -> System.out.println(a));
	}
}

/*
 * 출력 결과 :
 * This is a java book
 * Java8 supports lambda expressions
 * */