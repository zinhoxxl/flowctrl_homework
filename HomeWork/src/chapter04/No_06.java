package chapter04;

/*
 * 문제
 * for문을 이용해서 실행 결과와 같은 삼각형을 출력하는 코드를 작성해보세요
 * */
public class No_06 {
	public static void main(String[] args) {

		int x = 0;
		int y = 0;
		
		for (x = 1; x <= 5; x++) { // 총 다섯줄
			for (y = 1; y <= x; y++) {
				System.out.print("*"); // 띄어쓰기 안되게 print로
			}
			System.out.println(); // 중첩 for문이 한바퀴돌면 한칸 내리기
		}
		
	}
}
