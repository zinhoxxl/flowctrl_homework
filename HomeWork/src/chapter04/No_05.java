package chapter04;

/*
 * 문제
 * 중첩 for문을 이용하여 방정식 4x + 5y = 60의 모든 해를 구해서 (x,y) 형태로 출력해보세요.
 * 단, x와 y는 10이하의 자연수 입니다
 * */
public class No_05 {
	public static void main(String[] args) {

		int x = 0;
		int y = 0;
		
		for (x = 1; x <= 10; x++) {	//x가 10이될때까지 
			for (y = 1; y <= 10; y++) {	// y도 10이 될때까지
				if((4 * x) + (5 * y) == 60)
					System.out.println("(" + x + ", " + y + ")"); // (5,8) (10,4) 출력
			}
		}
		
	}
}
