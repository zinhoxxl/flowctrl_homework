package chapter04;

/*
 * 문제
 * for문을 이용하여 1부터 100까지의 정수 중에서 3의 배수의 총합을 구하는 코드를 작성해보세요
 * */
public class No_03 {
	public static void main(String[] args) {

		int sum = 0; // 3의 배수의 총합을 담을 sum

		for (int i = 1; i <= 100; i++) { // 1부터 100까지 반복
			if(i%3 == 0)	// 3으로 나눴을때 0이나옴 => 3의 배수
				sum += i; // sum에 저장
		}
		System.out.println("3의 배수의 합 : " + sum); // 1683
	}
}
