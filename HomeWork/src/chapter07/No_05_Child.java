package chapter07;

/*
 * 문제
 * Parent 클래스를 상속해서 Child 클래스를 다음과 같이 작성했는데, 
 * Child 클래스의 생성자에서 컴파일 에러가 발생했다. 그 이유를 설명해보세요.
 * */
public class No_05_Child {
	private int studentNo;

	public No_05_Child(String name, int studentNo) {
		// this.name = name;
		this.studentNo = studentNo;
	}
} // 정답 : 자식클래스에서 부모 클래스를 호출 안함
