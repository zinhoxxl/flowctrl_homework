package chapter07;

/*
 * 문제
 * Parent 클래스를 상속받아 Child 클래스를 다음과 같이 작성했다. 
 * ChildExample 클래스를 실행했을 때 호출되는 각 클래스의 생성자의 순서를 생각하면서 출력 결과를 작성해보세요.
 * */
public class No_06_ChildExample {
	public static void main(String[] args) {
		No_06_Child child = new No_06_Child();
	}
}

// 정답 : Parent(String nation) call => Parent() call => Child(String name) call => Child() call