package chapter07;

/*
 * 문제
 * Tire클래스를 상속받아 SnowTire 클래스를 다음과 같이 작성했습니다. 
 * SnowTireExample클래스를 실행 했을 때 출력 결과는 무엇일까요?
 * */
public class No_07_SnowTireExample {
	public static void main(String[] args) {
		No_07_SnowTire snowTire = new No_07_SnowTire();
		No_07_Tire tire = snowTire;

		snowTire.run();
		tire.run();
	}
}
 // 정답 : 스노우 타이어가 굴러갑니다. , 스노우 타이어가 굴러갑니다.