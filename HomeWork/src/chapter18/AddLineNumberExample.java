package chapter18;

import java.io.BufferedReader;
import java.io.FileReader;

/*
 * 문제 7번
 * */
public class AddLineNumberExample {
	public static void main(String[] args) throws Exception {
		String filePath = "C:JavaProgramming/source/chap18/src/sec05/exam04_bufferdreader/BufferReaderExample.java";
		
		//작성 위치
		FileReader fr = new FileReader(filePath);
		BufferedReader br = new BufferedReader(fr);
		
		int rowNumber = 0;
		String rowData;
		while( (rowData = br.readLine()) != null) {
			System.out.println(++rowNumber + ":" + rowData);
		}
		
		br.close(); fr.close();
	}
}
