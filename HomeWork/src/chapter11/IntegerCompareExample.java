package chapter11;

/*
 * 문제 11번
 * 숫자 100과 300으로 각각 박싱된 Integer 객체를 == 연산자로 비교하였습니다. 
 * 100을 박싱한 Integer 객체는 true가 나오는데, 
 * 300을 박싱한 Integer 객체는 false가 나오는 이유를 설명하세요.
 * */
public class IntegerCompareExample {
	 
    public static void main(String[] args) {
        Integer obj1 = 100;
        Integer obj2 = 100;
        Integer obj3 = 300;
        Integer obj4 = 300;
        
        System.out.println( obj1 == obj2);
        System.out.println( obj3 == obj4);
    }
}

// 이유 : int는 -128 ~ 127 사이라면 내부의 값을 비교 가능하지만 범위를 벗어나면 객체의 번지를 비교한다.
