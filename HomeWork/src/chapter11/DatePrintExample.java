package chapter11;

/*
 * 문제 13번
 * SimpleDateFormat 클래스를 이용해서 오늘의 날짜를 다음과 같이 출력하는 코드를 작성하세요.
 * */
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatePrintExample {
	public static void main(String[] args) {
		Date now = new Date();
		System.out.println(now);
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY년 MM월 DD일 E요일 k시 m분");
		System.out.println(sdf.format(now));
	}
}
