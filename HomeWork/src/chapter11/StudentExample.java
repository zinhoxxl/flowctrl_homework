package chapter11;

/*
 * 문제 3번
 * Student 클래스를 작성하되, Object의 equals() 와 hashCode()를 오버라이딩해서 
 * Student의 학번(studentNum)이 같으면 동등 객체가 될 수 있도록 해보세요. 
 * hashCode()의 리턴값은 studentNum 필드값의 해시코드를 리턴하도록 하세요.
 * */
import java.util.HashMap;

public class StudentExample {

	public static void main(String[] args) {
		HashMap<Student, String> hashMap = new HashMap<Student, String>();

		hashMap.put(new Student("1"), "95");

		String score = hashMap.get(new Student("1"));
		System.out.println("1번 학생의 총점 : " + score);

	}

}
