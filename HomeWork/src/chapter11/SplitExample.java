package chapter11;

/*
 * 문제 8번
 * 다음 문자열에서 쉼표(,)로 구분되어 있는 문자열을 String의 split() 
 * 또는 StringTokenizer를 이용해서 분리해보세요.
 * */
import java.util.StringTokenizer;

public class SplitExample {

	public static void main(String[] args) {
		String str = "아이디,이름,패스워드";

		String[] str1 = str.split(",");
		for (int i = 0; i < str1.length; i++) {
			System.out.println(str1[i]);
		}
		System.out.println();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) {
			System.out.println(st.nextToken(","));
		}
	}
}
