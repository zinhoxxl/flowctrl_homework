package chapter17;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Student {
	private SimpleStringProperty name;
	private SimpleIntegerProperty korean;
	private SimpleIntegerProperty math;
	private SimpleIntegerProperty english;
	public Student() {
		this.name = new SimpleStringProperty();
		this.korean = new SimpleIntegerProperty();
		this.math = new SimpleIntegerProperty();
		this.english = new SimpleIntegerProperty();
	}
	public Student(String name, int korean, int math, int english) {
		this.name = new SimpleStringProperty();
		this.korean = new SimpleIntegerProperty();
		this.math = new SimpleIntegerProperty();
		this.english = new SimpleIntegerProperty();
	}
	public SimpleStringProperty getName() {
		return name;
	}
	public void setName(SimpleStringProperty name) {
		this.name = name;
	}
	public SimpleIntegerProperty getKorean() {
		return korean;
	}
	public void setKorean(SimpleIntegerProperty korean) {
		this.korean = korean;
	}
	public SimpleIntegerProperty getMath() {
		return math;
	}
	public void setMath(SimpleIntegerProperty math) {
		this.math = math;
	}
	public SimpleIntegerProperty getEnglish() {
		return english;
	}
	public void setEnglish(SimpleIntegerProperty english) {
		this.english = english;
	}
	
	
	
}
