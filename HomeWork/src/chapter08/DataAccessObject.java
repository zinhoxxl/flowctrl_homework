package chapter08;

/*
 * 문제 4번
 * DaoExample 클래스의 main() 메소드에서 dbWork() 메소드를 호출할 때 OracleDao와 MysqlDao 객체를 매개값으로 주고 호출했습니다. 
 * dbWork() 메소드는 두 객체를 모두 매개값으로 받기 위해 DataAccessObject 타입의 매개 변수를 가지고 있습니다. 
 * 실행 결과를 보고 DataAccessObject 인터페이스와 OracleDao, MySqlDao 구현 클래스를 각각 작성해보세요.
 * */
public interface DataAccessObject {
	void select();
	void insert();
	void update();
	void delete();
}
