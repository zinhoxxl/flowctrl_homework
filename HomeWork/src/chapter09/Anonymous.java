package chapter09;

/*
 * 문제 5번
 * AnonymousExample 클래스의 실행 결과를 보고 
 * Vehicle 인터페이스의 익명 구현 클래스 객체를 이용해서 
 * 필드, 로컬 변수의 초기값과 메소드의 매개값을 대입해 보세요.
 * */
public class Anonymous {
	Vehicle field = new Vehicle() {
		@Override
		public void run() {
			System.out.println("자전거가 달립니다.");
		}
	};

	void method1() {
		Vehicle localVar = new Vehicle() {
			@Override
			public void run() {
				System.out.println("승용차가 달립니다.");
			}
		};
		localVar.run();
	}

	void method2(Vehicle v) {
		v.run();
	}

}
