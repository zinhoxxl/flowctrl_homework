package chapter09;

/*
 * 문제 4번
 * 다음과 같이 Car 클래스 내부에 Tire와 Engine이 멤버 클래스로 선언되어 있습니다. 
 * 외부 클래스(NestedClassExample)에서 멤버 클래스의 객체를 생성하는 코드를 작성해 보세요.
 * */
public class NestedClassExample {
	public static void main(String[] args) {
		Car myCar = new Car();
		
		Car.Tire tire = myCar.new Tire();
		Car.Engine engine = new Car.Engine();
	}

}
