package chapter14;

/*
 * 문제 6번
 * 다음은 학생의 영어 평균 점수와 수학 평균 점수를 계산하는 코드입니다. 
 * avg() 메소드를 선언해보세요.
 * */
import java.util.function.ToIntFunction;

public class LamdaExample_06 {
	private static Student[] students = { new Student("홍길동", 90, 96), new Student("신용권", 95, 93) };

	public static double avg(ToIntFunction<Student> function) {
		int sum = 0;
		for (Student student : students) {
			sum += function.applyAsInt(student);
		}
		double avg = (double) sum / students.length;
		return avg;
	}

	public static void main(String[] args) {
		double englishAvg = avg(s -> s.getEnglishScore());
		System.out.println("영어 평균 점수 : " + englishAvg); // 영어 평균 점수 : 92.5

		double mathAvg = avg(s -> s.getMathScore());
		System.out.println("수학 평균 점수 : " + mathAvg); // 수학 평균 점수 : 94.5
	}

	public static class Student {
		private String name;
		private int englishScore;
		private int mathScore;

		public Student(String name, int englishScore, int mathScore) {
			this.name = name;
			this.englishScore = englishScore;
			this.mathScore = mathScore;
		}

		public String getName() {
			return name;
		}

		public int getEnglishScore() {
			return englishScore;
		}

		public int getMathScore() {
			return mathScore;
		}
	}
}
