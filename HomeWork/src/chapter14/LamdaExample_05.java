package chapter14;

/*
 * 문제 5번
 * 다음은 배열 항목 중에 최대값 또는 최소값을 찾는 코드입니다. 
 * maxOrMin() 메소드의 매개값을 람다식으로 기술해보세요.
 * */
import java.util.function.IntBinaryOperator;

public class LamdaExample_05 {
	private static int[] scores = { 10, 50, 3 };

	public static int maxOrMin(IntBinaryOperator operator) {
		int result = scores[0];
		for (int score : scores) {
			result = operator.applyAsInt(result, score);
		}
		return result;
	}

	public static void main(String[] args) {
		int max = maxOrMin((a, b) -> (a >= b) ? a : b);
		System.out.println("최대값 : " + max); // 최대값 : 50
		
		max = maxOrMin(new IntBinaryOperator() {
			@Override
			public int applyAsInt(int a, int b) {
				return (a > b) ? a : b;
			}
		});
		int min = maxOrMin((a, b) -> (a < b) ? a : b);
		System.out.println("최소값 : " + min); // 최소값 : 3
	}
}
