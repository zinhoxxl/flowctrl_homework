package chapter14;

/*
 * 문제 4번
 * 다음 코드는 컴파일 에러가 발생합니다. 그 이유가 무엇입니까?
 * */
import java.util.function.IntSupplier;

public class LamdaExample_04 {
	public static int method(int x, int y) {
		IntSupplier supplier = () -> {
			int x1 = x * 10;
			int result = x1 + y;
			return result;
		};
		int result = supplier.getAsInt();
		return result;
	}

	public static void main(String[] args) {
		System.out.println(method(3, 5));
	}
}
/*
 * 정답 : 람다식 안에 선언된 매개변수와 로컬 변수는 final 특성을 가지고 있어 데이터 변경이 불가능
 */
