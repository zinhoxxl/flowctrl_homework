package chapter05;

/*
 * 문제
 * 주어진 배열의 전체 항목의 합과 평균값을 구해보세요
 * (중첩 for문을 이용하세요)
 * */
public class No_08 {
	public static void main(String[] args) {
		
		int[][] array = {
				{ 95, 86 }, 
				{ 83, 92, 96 }, 
				{ 78, 83, 93, 87, 88 } 	
		};
		
		int sum = 0;
		double avg = 0.0;
		
		int num = 0;
		for (int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				sum += array[i][j];
			}
			num += array[i].length;
		}
		avg = (double)sum / num;
		
		System.out.println("sum : " + sum); // 881
		System.out.println("avg : " + avg); // 88.1
	}
}
