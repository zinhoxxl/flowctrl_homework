package chapter03;

/*
 * 문제
 * 다음 코드를 실행했을 때 출력 결과는 무엇입니까?
 * */
public class No_02 { 
	public static void main(String[] args) {
		int x = 10;
		int y =20;
		// ++x 는 먼저 1증감 (값 : 11), y-- 는 먼저 사용 하고 난 후 1감소 (값 : 20) 
		int z = (++x) + (y--); // z = (10+1) + 20
		System.out.println(z);	// 정답 : 31
	}

}
