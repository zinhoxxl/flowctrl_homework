package chapter03;

/*
 * 문제
 * 다음 코드를 실행했을 때 출력 결과는 무엇입니까?
 * */
public class No_03 {
	public static void main(String[] args) {
		int score = 85; 
		String result = (!(score>90))? "가":"나"; // score가 90보다 크지 않으면 "가" 출력, 크다면 "나" 출력 
		System.out.println(result); // 정답 : 가 (85 > 90 == false)
	}

}
