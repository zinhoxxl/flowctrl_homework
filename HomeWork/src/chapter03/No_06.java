package chapter03;

/*
 * 문제
 * 다음 코드는 사다리꼴의 넓이를 구하는 코드입니다. 정확히 소수자릿수가 나올 수 있도록 
 * (#1) 에 알맞은 코드를 작성하세요
 * */
public class No_06 {
	public static void main(String[] args) {
		
		int lengthTop = 4;
		int lengthBottom = 10;
		int height = 7;
		// 사다리꼴 넓이 구하는 공식 : (윗변 + 아랫변) * 높이 / 2
		double area = (double)(lengthTop + lengthBottom) * height / 2;	// 정확한 값 얻기 위해 실수타입으로 형변환
		System.out.println(area); 	// 정답 : 49.0
		
	}

}
