package chapter03;

/*
 * 문제
 * 다음 코드는 비교 연산자와 논리 연산자의 복합 연산식입니다. 
 * 연산식의 출력 결과를 괄호() 속에 넣으세요
 * */
public class No_07 {
	public static void main(String[] args) {
		
		int x = 10;
		int y = 5;
		
		/*
		 * 논리 연산자
		 * && : 피연산자 모두가 true일 경우 => true
		 * || : 피연산자 중 하나만 true여도 => true
		 * */
		System.out.println( (x>7) && (y<=5) ); 			// 둘다 true  => true 출력
		System.out.println( (x%3 == 2) || (y%2 != 1) );	// 둘다 false => false 출력
		
	}

}
