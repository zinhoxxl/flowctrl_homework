package chapter06;

/*
 * 문제
 * 16번 문제에서는 Printer 객체를 생성하고 println() 메소드를 생성했습니다.
 * Printer 객체를 생성하지 않고 PrinterExample 클래스에서 다음과 같이 호출하려면
 * Printer 클래스를 어떻게 수정하면 될까요?
 * */
public class No_17_Printer {
	static void println(int num) {
		System.out.println(num);
	}

	static void println(boolean bool) {
		System.out.println(bool);
	}

	static void println(double dou) {
		System.out.println(dou);
	}

	static void println(String str) {
		System.out.println(str);
	}

}
