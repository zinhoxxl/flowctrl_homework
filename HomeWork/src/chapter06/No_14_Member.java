package chapter06;

/*
 * [ 14번 ]
 * 문제
 * 위에서 작성한 Member 클래스에 생성자를 추가하려고 합니다.
 * 다음과 같이 Member 객체를 생성할 때 name 필드와 id 필드를 외부에서 받은 값으로 
 * 초기화하려면 생성자를 어떻게 선언해야 합니까?
 * */
public class No_14_Member {

	String name;
	String id;
	String password;
	int age;

	public No_14_Member(String name, String id) {
		this.name = name;
		this.id = id;
	}
}
