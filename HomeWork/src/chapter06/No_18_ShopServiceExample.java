package chapter06;

/*
 * 문제
 * ShopService 객체를 싱글톤으로 만들고 싶습니다. ShopServiceExample 클래스에서 ShopService의 getInstance()
 * 메소드로 싱글톤을 얻을 수 있도록 ShopService 클래스를 작성해보세요
 * */
public class No_18_ShopServiceExample {
	public static void main(String[] args) {
		No_18_ShopService obj1 = No_18_ShopService.getInstance();
		No_18_ShopService obj2 = No_18_ShopService.getInstance();
		
		if(obj1 == obj2) {
			System.out.println("같은 ShopService 객체 입니다."); // 정답
		} else {
			System.out.println("다른 ShopService 객체 입니다.");
		}
	}
}
