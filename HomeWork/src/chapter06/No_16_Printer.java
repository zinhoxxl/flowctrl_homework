package chapter06;

/*
 * 문제
 * MembmerService 클래스에 login() 메소드와  logout() 메소드를 선언하려고 합니다.
 * login() 메소드를 호출 할 때에는 매개값을 id와 password를 제공하고,
 * logout()메소드는 id만 매개값으로 제공합니다.
 * MebmerService 클래스와 login(), logout() 메소드를 선언해보세요
 * */
public class No_16_Printer {
	void println(int num) {
		System.out.println(num);
	}

	void println(boolean bool) {
		System.out.println(bool);
	}

	void println(double dou) {
		System.out.println(dou);
	}

	void println(String str) {
		System.out.println(str);
	}

}
