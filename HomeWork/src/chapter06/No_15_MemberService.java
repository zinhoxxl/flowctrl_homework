package chapter06;

/*
 * 문제
 * MembmerService 클래스에 login() 메소드와  logout() 메소드를 선언하려고 합니다.
 * login() 메소드를 호출 할 때에는 매개값을 id와 password를 제공하고,
 * logout()메소드는 id만 매개값으로 제공합니다.
 * MebmerService 클래스와 login(), logout() 메소드를 선언해보세요
 * */
public class No_15_MemberService {
	boolean login(String id, String password) {
		if ("hong".equals(id) && "12345".equals(password))
			return true;
		else
			return false;
	}

	void logout(String id) {
		System.out.println("로그아웃 되었습니다.");
	}

}
