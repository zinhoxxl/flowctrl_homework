package chapter06;

/*
 * 문제
 * ShopService 객체를 싱글톤으로 만들고 싶습니다. ShopServiceExample 클래스에서 ShopService의 getInstance()
 * 메소드로 싱글톤을 얻을 수 있도록 ShopService 클래스를 작성해보세요
 * */
public class No_18_ShopService {
	private static No_18_ShopService shopService = new No_18_ShopService();
	
	private No_18_ShopService() {}
	
	static No_18_ShopService getInstance() {
		return shopService;
	}
}
