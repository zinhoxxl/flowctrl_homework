package chapter10;

/*
 * 문제 7번
 * 로그인 기능을 Member 클래스의 login() 메소드에서 구현하려고 합니다. 
 * 존재하지 않는 ID를 입력했을 경우 NotExistIDException을 발생시키고, 
 * 잘못된 패스워드를 입력했을 경우 WrongPasswordException을 발생시키려고 합니다. 
 * LoginExample의 실행 결과를 보고 빈칸을 채워보세요.
 * */
public class LoginExample {
	public static void main(String[] args) {
		try {
			login("white", "12345");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			login("blue", "54321");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void login(String id, String password) throws NotExistIDException, WrongPasswordException {
		
		//id가 "blue"가 아니라면 NotExistIDException 발생시킴
		if (!id.equals("blue")) {
			throw new NotExistIDException("아이디가 존재하지 않습니다.");
		}
		
		//password가 "1234"가 아니라면 WrongPasswordException 발생시킴
		if (!password.equals("12345")) {
			throw new WrongPasswordException("패스워드가 틀립니다.");
		}
	}
}
// 출력 결과 : 
/*
 * 아이디가 존재하지 않습니다. 
 * 패스워드가 틀립니다.
 */
