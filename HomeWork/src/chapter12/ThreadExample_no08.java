package chapter12;

/*
 * 문제 8번
 * 메인 스레드에서 1초 후 MovieThread의 interrupt() 
 * 메소드를 호출해서 MovieTread를 안전하게 종료하고 싶습니다.
 * 비어 있는 부분에 적당한 코드를 작성해보세요.
 * */
public class ThreadExample_no08 {
	public static void main(String[] args) {
		Thread thread = new MovieThread_no08();
		thread.start();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {

		}
		thread.interrupt();
	}
}
