package chapter12;

/*
 * 문제 2번
 * 동영상과 음악을 재생하기 위해 두 가지 스레드를 실행하려고 합니다. 
 * 비어 있는 부분에 적당한 코드를 넣어보세요.
 * */
public class ThreadExample {
    public static void main(String[] args) {
        Thread thread1 = new MovieThread();
        thread1.start();
        
        Thread thread2 = new Thread(new MusicRunnalbe());
        thread2.start();
    }
}

