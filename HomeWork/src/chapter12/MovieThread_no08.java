package chapter12;

/*
 * 문제 8번
 * 메인 스레드에서 1초 후 MovieThread의 interrupt() 
 * 메소드를 호출해서 MovieTread를 안전하게 종료하고 싶습니다.
 * 비어 있는 부분에 적당한 코드를 작성해보세요.
 * */
public class MovieThread_no08 extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("동영상을 재생합니다.");
			if (Thread.interrupted()) {
				break;
			}
		}
	}
}
