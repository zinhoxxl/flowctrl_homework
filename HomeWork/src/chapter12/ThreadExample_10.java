package chapter12;

/*
 * 문제 10번
 * 메인 스레드가 종료하면 MovieThread도 같이 종료되게 만들고 싶습니다. 
 * 비어 있는 부분에 적당한 코드를 넣어보세요
 * */
public class ThreadExample_10 {
    public static void main(String[] args) {
        Thread thread = new MovieThread();
        thread.setDaemon(true);
        thread.start();
        
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            
        }
    }
}

