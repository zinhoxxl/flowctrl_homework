package chapter12;

/*
 * 문제 2번
 * 동영상과 음악을 재생하기 위해 두 가지 스레드를 실행하려고 합니다. 
 * 비어 있는 부분에 적당한 코드를 넣어보세요.
 * */
public class MusicRunnalbe implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("음악을 재생합니다.");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
	}

}
