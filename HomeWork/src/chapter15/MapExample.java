package chapter15;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/*
 * 문제 9번
 * HashMap에 아이디(String)와 점수(Integer)가 저장되어 있습니다. 
 * 실행 결과와 같이 평균 점수를 출력하고, 최고 점수와 최고 점수를 받은 아이디를 출력해보세요.
 * */

public class MapExample {
	public static void main(String[] args) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("blue", 96);
		map.put("hong", 86);
		map.put("white", 92);

		String name = null;
		int maxScore = 0;
		int totalScore = 0;

		Set<String> keySet = map.keySet();
		Iterator<String> iterator = keySet.iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			int value = map.get(key);
			totalScore += value;
			maxScore = (maxScore < value) ? value : maxScore;
			name = (maxScore <= map.get(key)) ? key : name;
		}

		System.out.println("평균 점수 : " + totalScore / map.size()); // 평균 점수 : 91
		System.out.println("최고 점수 : " + maxScore); // 최고 점수 : 96
		System.out.println("최고 점수를 받은 아이디 : " + name); // 최고 점수를 받은 아이디 : blue
		System.out.println();
		totalScore = 0;
		maxScore = 0;
		name = null;
	}
}
