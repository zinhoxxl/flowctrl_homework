package chapter15;

import java.util.TreeSet;

/*
 * 문제 10번
 * TreeSet에 Student 객체를 저장하려고 합니다. 
 * Student의 score 필드값으로 자동 정렬하도록 구현하고 싶습니다. 
 * TreeSet의 last() 메소드를 호출했을 때 가장 높은 score의 Student객체가 
 * 리턴되도록 Student 클래스를 완성해보세요.
 * */

// 가장 높은 점수 출력
public class TreeSetExample {
	public static void main(String[] args) {
		TreeSet<Student_10> treeSet = new TreeSet<Student_10>();
		treeSet.add(new Student_10("blue", 96));
		treeSet.add(new Student_10("hong", 86));
		treeSet.add(new Student_10("white", 92));

		Student_10 student = treeSet.last();
		System.out.println("최고 점수 : " + student.score); // 최고 점수 : 96
		System.out.println("최고 점수를 받은 아이디 : " + student.id); // 최고 점수를 받은 아이디 : blue
	}
}

class Student_10 implements Comparable<Student_10> {
	public String id;
	public int score;

	public Student_10(String id, int score) {
		this.id = id;
		this.score = score;
	}

	@Override
	public int compareTo(Student_10 o) {
		if (o.score < score)
			return 1;
		else if (o.score == score)
			return 0;
		else
			return -1;

	}
}
