package chapter15;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
 * 문제 8번
 * HashSet에 Student 객체를 저장하려고 합니다. 
 * 학번이 같으면 동일한 Student라고 가정하고 중복 저장이 되지 않도록 하고 싶습니다. 
 * Student 클래스에서 재정의해야 하는 hashCode()와 equals() 메소드의 내용을 채워보세요. 
 * Student의 해시코드는 학번이라고 가정합니다.
 * */

// Student 중복 저장 방지
public class HashSetExample {
	public static void main(String[] args) {
		Set<Student> set = new HashSet<Student>();

		set.add(new Student(1, "홍길동"));
		set.add(new Student(2, "신용권"));
		set.add(new Student(1, "조민우")); // 학번이 저장되지 않음

		Iterator<Student> iterator = set.iterator();
		while (iterator.hasNext()) {
			Student student = iterator.next();
			System.out.println(student.studentNum + " : " + student.name);
		}
	}
}

/*
 * 출력 결과 :
 * 1 : 홍길동
 * 2 : 신용권
 * */

// hashCode()와 equals() 재정의
class Student {
	public int studentNum;
	public String name;

	public Student(int studentNum, String name) {
		this.studentNum = studentNum;
		this.name = name;
	}

	@Override
	public int hashCode() {
		return studentNum;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Student) {
			Student student = (Student) obj;
			if (student.studentNum == studentNum)
				return true;
		}
		return false;
	}
}
